package org.example;

import java.util.Stack;

public class Parser {

    public void showMenu(){
        System.out.println("*******************");
        System.out.println("DEFINE");
        System.out.println("SOLVE");
        System.out.println("ALL");
        System.out.println("SOLVE");
        System.out.println("*******************");
    }

    public boolean evaluate(Stack<Boolean> parameters, Stack<String> operators){
        boolean result;
        boolean rightOperand;

        result =  parameters.pop();
        while (!operators.isEmpty()){
            rightOperand =  parameters.pop();
            String operator = operators.pop();

            if(operator.equals("&")){
                if((!operators.isEmpty()) && operators.peek().equals("!")){
                    result = result & (!rightOperand);
                    operators.pop();
                }
                else{
                    result = result & rightOperand;
                }
            }

            if(operator.equals("|")){
                if((!operators.isEmpty()) && operators.peek().equals("!")){
                    result = result | (!rightOperand);
                    operators.pop();
                }
                else {
                    result = result | rightOperand;
                }
            }
        }
        return result;
    }

    public Stack<String> getParameters(String input, int openParenIndex, int closeParenIndex) {
        String parameterString = subString(input,openParenIndex + 1, closeParenIndex).trim();
        String[] tokens = parameterString.split(",");
        Stack<String> parameters = new Stack<>() {};
        Stack<String> upsideParametersStack = new Stack<>();

        for(String token : tokens){
            parameters.push(token);
        }

        do{
            upsideParametersStack.push(parameters.pop());
        }while (!parameters.isEmpty());

        return upsideParametersStack;
    }

    public Stack<String> getOperators(String input, int openQuotationMark, int closeQuotationMark){
        Stack<String> operators = new Stack<>();
        Stack<String> upsideOperatorsStack = new Stack<>();
        String expression = subString(input,openQuotationMark + 1, closeQuotationMark);
        System.out.println("Expression -> " + expression);

        for (String operator : expression.split("")){
            switch (operator) {
                case "&", "|", "!" -> operators.push(operator);
            }
        }

        do{
            upsideOperatorsStack.push(operators.pop());
        }while(!operators.isEmpty());

        return upsideOperatorsStack;
    }

    public String getFunctionName(String input, int openParenIndex) {
        return subString(input, indexOF(input, " "), openParenIndex).trim();
    }

    public String subString(String str, int startIndex, int endIndex) {
        if (startIndex < 0 || endIndex > str.length() || startIndex > endIndex) {
            throw new IndexOutOfBoundsException("Invalid start or end index");
        }
        char[] chars = str.toCharArray();
        char[] resultChars = new char[endIndex - startIndex];
        for (int i = startIndex; i < endIndex; i++) {
            resultChars[i - startIndex] = chars[i];
        }
        return new String(resultChars);
    }

    public int indexOF(String str, String substring) {
        char[] strChars = str.toCharArray();
        char[] subChars = substring.toCharArray();

        for (int i = 0; i <= strChars.length - subChars.length; i++) {
            boolean match = true;
            for (int j = 0; j < subChars.length; j++) {
                if (strChars[i + j] != subChars[j]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return i;
            }
        }
        return -1;
    }

    public int lastIndexOF(String str, String substring) {
        char[] strChars = str.toCharArray();
        char[] subChars = substring.toCharArray();
        for (int i = strChars.length - subChars.length; i >= 0; i--) {
            boolean match = true;
            for (int j = 0; j < subChars.length; j++) {
                if (strChars[i + j] != subChars[j]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return i;
            }
        }
        return -1;
    }

}
