package org.example;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Stack;

public class Main {

    public static final String DEFINE = "DEFINE";
    public static final String SOLVE = "SOLVE";
    public static final String ALL = "ALL";
    public static final String FIND = "FIND";

    public static void main(String[] args) throws IOException {
        Parser parser = new Parser();
        ExpressionTree tree = new ExpressionTree();
        FileHelper fileHelper = new FileHelper();
        final HashMap<String, Stack<String>> functionsAndParameters = new HashMap<>();
        final HashMap<String, Boolean> functionsAndResults = new HashMap<>();

        Stack<String> upsideParameters;
        Stack<String> upsideOperators = new Stack<>();
        String functionName;
        String functionDefinition = null;
        boolean result = true;

        while (true) {
            parser.showMenu();

            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            String function = parser.subString(input, 0, parser.indexOF(input, " "));

            int openParenIndex = parser.indexOF(input, "(");
            int closeParenIndex = parser.lastIndexOF(input, ")");
            int openQuotationMark = parser.indexOF(input, "\"");
            int closeQuotationMark = parser.lastIndexOF(input, "\"");


            Stack<Boolean> parametersInBooleanValues = new Stack<>();
            Stack<String> paramsInSolve = parser.getParameters(input, openParenIndex, closeParenIndex);

            if (function.equals(DEFINE)) {
                if (openParenIndex >= 0) {
                    functionName = parser.getFunctionName(input, openParenIndex);
                    functionDefinition = input.substring("DEFINE ".length()).trim();

                    Stack<String> paramsInDefine = parser.getParameters(input, openParenIndex, closeParenIndex);
                    String expression = parser.subString(input, openQuotationMark + 1, closeQuotationMark);

                    if (closeParenIndex < 0) {
                        System.out.println("Invalid input: missing closing parenthesis");
                        break;
                    }

                    boolean checkDefinedParameters = checkIfParametersAreDefinedCorrect(paramsInDefine, expression);

                    if(!checkDefinedParameters){
                        break;
                    }

                    upsideParameters = parser.getParameters(input, openParenIndex, closeParenIndex);
                    upsideOperators = parser.getOperators(input, openQuotationMark, closeQuotationMark);

                    functionsAndParameters.put(functionName, upsideParameters);
                    fileHelper.save(functionsAndParameters);
                }

            }
            if (function.equals(SOLVE)) {
                if (openParenIndex >= 0) {
                    functionName = parser.getFunctionName(input, openParenIndex);

                    if (functionsAndParameters.containsKey(functionName)) {
                        convertParametersToBooleanValues(parametersInBooleanValues, paramsInSolve);
                    }
                    result = parser.evaluate(parametersInBooleanValues, upsideOperators);

                    System.out.println("The result of function -> " + functionDefinition + " -> " + result);
                }
            }
        }
    }

    private static boolean checkIfParametersAreDefinedCorrect(Stack<String> paramsInDefine, String expression) {
        String output = "";
        for(int i = 0; i < expression.length(); i++) {
            String ch = String.valueOf(expression.charAt(i));

            if(ch.equals(" ") | ch.equals("!") | ch.equals("|") | ch.equals("&")){
                continue;
            }
            else {
                output += ch;
            }
        }

        for (int i = 0; i < output.length(); i++){
            String ch = String.valueOf(output.charAt(i));

            if(!paramsInDefine.contains(ch)){
                System.out.println("Parameter " + ch + " is not defined");

                return false;
            }
        }
        return true;
    }

    private static void convertParametersToBooleanValues(Stack<Boolean> parametersInBooleanValues, Stack<String> paramsInSolve) {
        for (String param : paramsInSolve) {
            if (param.equals("1")) {
                parametersInBooleanValues.push(true);
            } else if (param.equals("0")) {
                parametersInBooleanValues.push(false);
            } else {
                System.out.println("Invalid parameter! ");
            }
        }
    }

}

