package org.example;

public class Stack<T> {
    private T[] data;
    private int size;
    private int top;

    public Stack() {
    }

    public boolean push(T value) {
        if (top == size - 1) {
            return false; // stack is full
        }
        top++;
        data[top] = value;
        return true;
    }

    public T pop() {
        if (top == -1) {
            throw new IllegalStateException("Stack is empty");
        }
        T value = data[top];
        top--;
        return value;
    }

    public T peek() {
        if (top == -1) {
            throw new IllegalStateException("Stack is empty");
        }
        return data[top];
    }

    public boolean isEmpty() {
        return top == -1;
    }
}