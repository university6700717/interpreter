package org.example;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class FileHelper {

    public void save(HashMap<String, Stack<String>> map) throws IOException {
        String outputFilePath = "C:/Users/aliss/Documents/hashmap.txt";
        File file = new File(outputFilePath);

        BufferedWriter bf = null;;

        try{
            bf = new BufferedWriter( new FileWriter(file) );

            for(Map.Entry<String, Stack<String>> entry : map.entrySet()){
                bf.write( entry.getKey() + ":" + entry.getValue() );
                bf.newLine();
            }

            bf.flush();

        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally{

            try{
                bf.close();
            }
            catch(Exception e){}
        }
    }
}
