package org.example;

import java.util.Stack;

class Node {
    private final String value;
    private Node left;
    private Node right;

    public Node(String value) {
        this.value = value;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public String getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }
}


public class ExpressionTree {
    private static Stack<Node> stack = new Stack<>();
    public static Node buildTree(String expression) {

        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);

            if (ch == ' ') {
                continue;
            } else if (ch == '&' || ch == '|' || ch == '!') {
                Node node = new Node(Character.toString(ch));

                if (!stack.empty()) {
                    node.setRight(stack.pop());
                }

                if (!stack.empty()) {
                    node.setLeft(stack.pop());
                }

                stack.push(node);
            } else if (Character.isLetterOrDigit(ch)) {
                Node node = new Node(Character.toString(ch));
                stack.push(node);
            } else {
                throw new IllegalArgumentException("Invalid character: " + ch);
            }
        }

        if (stack.empty()) {
            return null;
        }

        Node root = stack.pop();

        if (!stack.empty()) {
            throw new IllegalArgumentException("Invalid expression");
        }

        return root;
    }

}



